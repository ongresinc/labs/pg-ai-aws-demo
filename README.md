# pg.ai lab on centos 8 and aws

## before start

This project uses [Pipenv](https://pypi.org/project/pipenv/) to set up the python dependencies, install the dependencies before start:

```bash
PIPENV_SKIP_LOCK=1 \
PIPENV_VENV_IN_PROJECT=1 \
PIPENV_IGNORE_VIRTUALENVS=1 \
pipenv install
```

then start a shell with all the installed packages:

```bash
pipenv shell
```

## creating the server

Run this commands to setup a new server:

```bash
ansible-playbook setup.yml
```

## cleanup

Run this commands to remove the server:

```bash
ansible-playbook cleanup.yml
```
